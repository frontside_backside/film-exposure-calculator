﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpCalc
{
    public static class ExposureCalc
    {
        private static int sensitive { get; set; }

        private static int[][] momentArr =
            {
                    new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 6, 5, 4, 5, 6, 9, 99, 99, 99, 99, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 5, 4, 4, 3, 4, 4, 5, 9, 99, 99, 99, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 99, 99, 8, 5, 4, 3, 2, 2, 2, 3, 4, 5, 8, 99, 99, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 99, 8, 5, 3, 2, 2, 1, 1, 1, 2, 2, 3, 5, 8, 99, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 8, 6, 4, 2, 1, 1, 1, 1, 1, 1, 1, 2, 4, 6, 8, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 7, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 7, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 7, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 7, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 8, 6, 4, 2, 1, 1, 1, 1, 1, 1, 1, 2, 4, 6, 8, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 99, 8, 5, 3, 2, 2, 1, 1, 1, 2, 2, 3, 5, 8, 99, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 99, 99, 8, 5, 4, 3, 2, 2, 2, 3, 4, 5, 8, 99, 99, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 5, 4, 4, 3, 4, 4, 5, 9, 99, 99, 99, 99, 99, 99},
                    new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 6, 5, 4, 5, 6, 9, 99, 99, 99, 99, 99, 99, 99}
            };

        private static int[] apertureArr =
        {
            2, 4, 6, 8, 10, 12, 14, 16
        };

        private static int[] sceneArr =
        {
            0, 0, 2, 3, 1, 4, 3, 5, 8, 10,
            4, 5, 8, 3, 4, 6, 8,
            6, 8, 10, 13, 11, 13, 17, 19,
            22, 26, 24, 28, 26, 30
        };

        private static int[] weatherArr =
        {
            1, 0, 2, 3, 5, 7
        };



        public static string getShutter(int apArg, int scArg, int wArg, int sensitive)
        {
            string shutter = "enter data";
            if (momentArr[DateTime.Now.Month - 1][DateTime.Now.Hour] < 99)
            {
                int sum = 1 +
                momentArr[DateTime.Now.Month - 1][DateTime.Now.Hour] +
                apertureArr[apArg] +
                sceneArr[scArg] +
                weatherArr[wArg] +
                sensitive;

                if (sum < 24)
                {
                    return "Close aperture";
                }
                else if (sum > 46)
                {
                    return "Open aperture";
                }
                else
                {
                    switch (sum)
                    {
                        case 24:
                            shutter = "1/1000";
                            break;
                        case 25:
                            shutter = "1/1000 или 1/500";
                            break;
                        case 26:
                            shutter = "1/500";
                            break;
                        case 27:
                            shutter = "1/500 или 1/250";
                            break;
                        case 28:
                            shutter = "1/250";
                            break;
                        case 29:
                            shutter = "1/250 или 1/125";
                            break;
                        case 30:
                            shutter = "1/125";
                            break;
                        case 31:
                            shutter = "1/125 или 1/60";
                            break;
                        case 32:
                            shutter = "1/60";
                            break;
                        case 33:
                            shutter = "1/60 или 1/30";
                            break;
                        case 34:
                            shutter = "1/30";
                            break;
                        case 35:
                            shutter = "1/30 или 1/15";
                            break;
                        case 36:
                            shutter = "1/15";
                            break;
                        case 37:
                            shutter = "1/15 или 1/8";
                            break;
                        case 38:
                            shutter = "1/8";
                            break;
                        case 39:
                            shutter = "1/8 или 1/4";
                            break;
                        case 40:
                            shutter = "1/4";
                            break;
                        case 41:
                            shutter = "1/4 или 1/2";
                            break;
                        case 42:
                            shutter = "1/2";
                            break;
                        case 43:
                            shutter = "1/2 или 1s";
                            break;
                        case 44:
                            shutter = "1s";
                            break;
                        case 45:
                            shutter = "1s or 2s";
                            break;
                        case 46:
                            shutter = "2s";
                            break;
                    }
                    return shutter;
                }
            }
            else
            {
                return "so dark";
            }
        }


    }
}
