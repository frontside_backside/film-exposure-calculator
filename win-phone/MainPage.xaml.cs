﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Collections.Generic;

namespace ExpCalc
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor

        int sensitive;

       
        //массив поправки на время суток
        int[][] momentArr = 
            {
                    new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 6, 5, 4, 5, 6, 9, 99, 99, 99, 99, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 5, 4, 4, 3, 4, 4, 5, 9, 99, 99, 99, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 99, 99, 8, 5, 4, 3, 2, 2, 2, 3, 4, 5, 8, 99, 99, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 99, 8, 5, 3, 2, 2, 1, 1, 1, 2, 2, 3, 5, 8, 99, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 8, 6, 4, 2, 1, 1, 1, 1, 1, 1, 1, 2, 4, 6, 8, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 7, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 7, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 7, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 7, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 8, 6, 4, 2, 1, 1, 1, 1, 1, 1, 1, 2, 4, 6, 8, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 99, 8, 5, 3, 2, 2, 1, 1, 1, 2, 2, 3, 5, 8, 99, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 99, 99, 8, 5, 4, 3, 2, 2, 2, 3, 4, 5, 8, 99, 99, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 5, 4, 4, 3, 4, 4, 5, 9, 99, 99, 99, 99, 99, 99},
					new int[]{99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 6, 5, 4, 5, 6, 9, 99, 99, 99, 99, 99, 99, 99}
            };

        int[] apertureArr =
        {
            2, 4, 6, 8, 10, 12, 14, 16
        };

        int[] sceneArr =
        {
            0, 0, 2, 3, 1, 4, 3, 5, 8, 10,
            4, 5, 8, 3, 4, 6, 8,
            6, 8, 10, 13, 11, 13, 17, 19,
            22, 26, 24, 28, 26, 30
        };

        int[] weatherArr =
        {
            1, 0, 2, 3, 5, 7
        };

        public string getShutter(int apArg,int scArg,int wArg, int sensitive)
        {
            string shutter = "enter data";
            if (momentArr[DateTime.Now.Month - 1][DateTime.Now.Hour] < 99)
             {
                int sum = 1 +
                momentArr[DateTime.Now.Month - 1][DateTime.Now.Hour] +
                apertureArr[apArg] +
                sceneArr[scArg] +
                weatherArr[wArg] +
                sensitive ;

                if (sum < 24)
                {
                    return "Close aperture";
                }
                else if (sum > 46)
                {
                    return "Open aperture";
                }
                else
                {
                    switch (sum)
                    {
                        case 24:
                            shutter = "1/1000";
                            break;
                        case 25:
                            shutter = "1/1000 или 1/500";
                            break;
                        case 26:
                            shutter = "1/500";
                            break;
                        case 27:
                            shutter = "1/500 или 1/250";
                            break;
                        case 28:
                            shutter = "1/250";
                            break;
                        case 29:
                            shutter = "1/250 или 1/125";
                            break;
                        case 30:
                            shutter = "1/125";
                            break;
                        case 31:
                            shutter = "1/125 или 1/60";
                            break;
                        case 32:
                            shutter = "1/60";
                            break;
                        case 33:
                            shutter = "1/60 или 1/30";
                            break;
                        case 34:
                            shutter = "1/30";
                            break;
                        case 35:
                            shutter = "1/30 или 1/15";
                            break;
                        case 36:
                            shutter = "1/15";
                            break;
                        case 37:
                            shutter = "1/15 или 1/8";
                            break;
                        case 38:
                            shutter = "1/8";
                            break;
                        case 39:
                            shutter = "1/8 или 1/4";
                            break;
                        case 40:
                            shutter = "1/4";
                            break;
                        case 41:
                            shutter = "1/4 или 1/2";
                            break;
                        case 42:
                            shutter = "1/2";
                            break;
                        case 43:
                            shutter = "1/2 или 1s";
                            break;
                        case 44:
                            shutter = "1s";
                            break;
                        case 45:
                            shutter = "1s or 2s";
                            break;
                        case 46:
                            shutter = "2s";
                            break;
                    }
                    return shutter;
                }
            } else
            {
                return "so dark";
            }          
        }

        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
           // weatherList.ItemsSource = weather;

        }
        

        private void iso1_Checked(object sender, RoutedEventArgs e)
        {
            sensitive = 7;
        }

        private void iso2_Checked(object sender, RoutedEventArgs e)
        {
            sensitive = 5;
        }

        private void iso3_Checked(object sender, RoutedEventArgs e)
        {
            sensitive = 3;
        }

        private void iso4_Checked(object sender, RoutedEventArgs e)
        {
            sensitive = 1;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            shutterLabel.Text = "Shutter: " + getShutter(apertureList.SelectedIndex,
                 sceneList.SelectedIndex, weatherList.SelectedIndex, sensitive);
        }

        
        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}