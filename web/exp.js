var expObj = (function(){ 
	const arrDateTime = [
		[99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 6, 5, 4, 5, 6, 9, 99, 99, 99, 99, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 5, 4, 4, 3, 4, 4, 5, 9, 99, 99, 99, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 99, 99, 8, 5, 4, 3, 2, 2, 2, 3, 4, 5, 8, 99, 99, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 99, 8, 5, 3, 2, 2, 1, 1, 1, 2, 2, 3, 5, 8, 99, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 8, 6, 4, 2, 1, 1, 1, 1, 1, 1, 1, 2, 4, 6, 8, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 7, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 7, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 7, 4, 3, 2, 1, 0, 0, 0, 0, 0, 1, 2, 3, 4, 7, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 8, 6, 4, 2, 1, 1, 1, 1, 1, 1, 1, 2, 4, 6, 8, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 99, 8, 5, 3, 2, 2, 1, 1, 1, 2, 2, 3, 5, 8, 99, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 99, 99, 8, 5, 4, 3, 2, 2, 2, 3, 4, 5, 8, 99, 99, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 5, 4, 4, 3, 4, 4, 5, 9, 99, 99, 99, 99, 99, 99],
		[99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 9, 6, 5, 4, 5, 6, 9, 99, 99, 99, 99, 99, 99, 99]
	];
	var moment;
	var shutter = '';

	function getSensitive() {
		const s100 = document.getElementById('s100').checked;
		const s200 = document.getElementById('s200').checked;
		const s400 = document.getElementById('s400').checked;
		const s800 = document.getElementById('s800').checked;
		var sensitive = (s100 === true) ? 7 :
			(s200 === true) ? 5 :
			(s400 === true) ? 3 :
			(s800 === true) ? 1 : 7;
		return sensitive;
	};

	function getAperture() {
		return document.getElementById('aperture').value;
	};

	function getScene() {
		return document.getElementById('scene').value;
	};

	function getWeather() {
		return document.getElementById('weather').value;
	};

	function setDateTime() {
		const now = new Date();
		var date = now.getMonth();
		var hour = now.getHours();
		moment = arrDateTime[date][hour];
	};

	function calcShutter() {
		setDateTime();
		if (moment < 99) {
			result = (+getSensitive()) + (+getAperture()) + (+getScene()) + (+getWeather()) + (+moment) + 1;
		} else {
			result = 'err';
		}
		return result;
	};

	function convertShutter(input) {
		if (input == 'err') {
			shutter = 'Солнце ушло спать!:(';
		} else if ((+input) < 24) {
			shutter = 'Прикройте диафрагму';
		} else if ((+input) > 46) {
			shutter = 'Откройте диафрагму';
		} else {
			switch (+input) {
				case 24:
					shutter = '1/1000';
					break;
				case 25:
					shutter = '1/1000 или 1/500';
					break;
				case 26:
					shutter = '1/500';
					break;
				case 27:
					shutter = '1/500 или 1/250';
					break;
				case 28:
					shutter = '1/250';
					break;
				case 29:
					shutter = '1/250 или 1/125';
					break;
				case 30:
					shutter = '1/125';
					break;
				case 31:
					shutter = '1/125 или 1/60';
					break;
				case 32:
					shutter = '1/60';
					break;
				case 33:
					shutter = '1/60 или 1/30';
					break;
				case 34:
					shutter = '1/30';
					break;
				case 35:
					shutter = '1/30 или 1/15';
					break;
				case 36:
					shutter = '1/15';
					break;
				case 37:
					shutter = '1/15 или 1/8';
					break;
				case 38:
					shutter = '1/8';
					break;
				case 39:
					shutter = '1/8 или 1/4';
					break;
				case 40:
					shutter = '1/4';
					break;
				case 41:
					shutter = '1/4 или 1/2';
					break;
				case 42:
					shutter = '1/2';
					break;
				case 43:
					shutter = '1/2 или 1\"';
					break;
				case 44:
					shutter = '1\"';
					break;
				case 45:
					shutter = '1\" или 2\"';
					break;
				case 46:
					shutter = '2\"';
					break;
			}
		}
	};

	return {
		show: function() {
			convertShutter(calcShutter());
			document.getElementById('result').innerHTML = shutter;
		}
	};
})();

document.querySelectorAll('input, select').forEach(el => el.onchange = expObj.show);